package dao;

public enum TransactionType {
    TRANSFER,
    WITHDRAW,
    DEPOSIT,
}
