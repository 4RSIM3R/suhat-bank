package dao;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
public class TransactionDao {

    private String from;
    private String to;
    private TransactionType transactionType;
    private int amount;
}
