import chain.SuhatChain;
import com.google.gson.GsonBuilder;
import dao.NasabahDao;
import dao.TransactionDao;
import dao.TransactionType;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    static ArrayList<SuhatChain> blockChain = new ArrayList<>();
    static ArrayList<NasabahDao> nasabahDaos = new ArrayList<>();
    static DateFormat dateFormat = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss");
    static Scanner sc = new Scanner(System.in);
    static NasabahDao pickedNasabah;

    static {
        nasabahDaos.add(new NasabahDao("123456", "john", "123456", 100));
        nasabahDaos.add(new NasabahDao("654321", "travolta", "654321", 300));
    }

    public static void main(String[] args) {

        System.out.println("=== Welcome To Suhat Bank ===");
        System.out.print("Please enter your pin : ");

        String inputPin = sc.next();

        Optional<NasabahDao> pickedNasabah = getNasabah(inputPin);

        if (pickedNasabah.isEmpty()) {
            System.out.println("The provided pin was wrong");
        } else {
            NasabahDao nasabah = pickedNasabah.get();
            promptMenu();
        }


    }

    public static Optional<NasabahDao> getNasabah(String inputPin) {
        List<NasabahDao> rawNasabah = nasabahDaos.stream()
                .filter(nasabahDao -> nasabahDao.getPin().equals(inputPin))
                .collect(Collectors.toList());

        if (rawNasabah.size() > 0) {
            pickedNasabah = rawNasabah.get(0);
            return Optional.of(pickedNasabah);
        } else {
            return Optional.empty();
        }
    }

    public static void promptMenu() {
        System.out.printf("Welcome %s Please Select Menu Below \n", pickedNasabah.getName());
        System.out.println("1. Check Balance");
        System.out.println("2. Deposit Savings");
        System.out.println("3. Withdraw");
        System.out.println("4. Transfer");
        System.out.println("5. Check Transaction History");
        System.out.println("6. Exit");
        System.out.print("Menu : ");

        int pickedMenu = sc.nextInt();

        if (pickedMenu == 1) {
            checkBalance();
        } else if (pickedMenu == 2) {
            depositSavings();
        } else if (pickedMenu == 3) {
            withdraw();
        } else if (pickedMenu == 4) {
            transfer();
        } else if (pickedMenu == 5) {
            checkHistory();
        } else {
            System.exit(0);
        }
    }

    public static void checkBalance() {
        System.out.println("");
        System.out.printf("Your current balance : $ %d \n", pickedNasabah.getBalance());
        System.out.println("");
        promptMenu();
    }

    public static void depositSavings() {
        System.out.print("\n");

        System.out.print("Please input deposit amount : ");
        int depositAmount = sc.nextInt();

        pickedNasabah.setBalance(pickedNasabah.getBalance() + depositAmount);
        TransactionDao transactionDao = new TransactionDao("deposit", pickedNasabah.getName(), TransactionType.DEPOSIT, depositAmount);
        SuhatChain newChain;

        if (blockChain.size() > 0) {
            String prevHash = blockChain.get(blockChain.size() - 1).prevHash;
            newChain = new SuhatChain(prevHash, transactionDao);
        } else {
            // Generate genesis block
            newChain = new SuhatChain("0", transactionDao);
        }

        blockChain.add(newChain);

        int currentBalance = pickedNasabah.getBalance();

        System.out.printf("Your total balance now : %d \n", currentBalance);

        promptMenu();

    }

    public static void withdraw() {

        System.out.print("Please input withdraw amount : ");
        int withDrawAmount = sc.nextInt();

        if (withDrawAmount > pickedNasabah.getBalance()) {
            System.out.println("The Withdrawal Amount Cannot Be More Than Your Balance");
            withdraw();
        } else {
            pickedNasabah.setBalance(pickedNasabah.getBalance() - withDrawAmount);

            TransactionDao transactionDao = new TransactionDao(pickedNasabah.getName(), "withdraw", TransactionType.WITHDRAW, withDrawAmount);

            SuhatChain newChain;

            if (blockChain.size() > 0) {
                String prevHash = blockChain.get(blockChain.size() - 1).prevHash;
                newChain = new SuhatChain(prevHash, transactionDao);
            } else {
                // Generate genesis block
                newChain = new SuhatChain("0", transactionDao);
            }

            blockChain.add(newChain);

            int currentBalance = pickedNasabah.getBalance();

            System.out.printf("Your total balance now : %d \n", currentBalance);

            promptMenu();

        }

    }

    public static void transfer() {

        System.out.print("Please input transfer amount : ");
        int transferAmount = sc.nextInt();

        if (transferAmount > pickedNasabah.getBalance()) {
            System.out.println("The Transfer Amount Cannot Be More Than Your Balance");
            transfer();
        } else {

            System.out.print("Please input account number : ");
            String accountNumber = sc.next();

            List<NasabahDao> rawNasabah = nasabahDaos
                    .stream()
                    .filter(nasabahDao -> nasabahDao.getAccountNumber().equals(accountNumber))
                    .collect(Collectors.toList());

            if (rawNasabah.size() <= 0) {
                System.out.println("Your Account Number Invalid");
            } else {

                NasabahDao targetNasabah = rawNasabah.get(0);

                TransactionDao transactionDao = new TransactionDao(pickedNasabah.getName(), targetNasabah.getName(), TransactionType.TRANSFER, transferAmount);

                targetNasabah.setBalance(targetNasabah.getBalance() + transferAmount);
                pickedNasabah.setBalance(pickedNasabah.getBalance() - transferAmount);

                SuhatChain newChain;

                if (blockChain.size() > 0) {
                    String prevHash = blockChain.get(blockChain.size() - 1).prevHash;
                    newChain = new SuhatChain(prevHash, transactionDao);
                } else {
                    // Generate genesis block
                    newChain = new SuhatChain("0", transactionDao);
                }

                blockChain.add(newChain);

                System.out.printf("Your total balance now : %d \n", pickedNasabah.getBalance());

                promptMenu();

            }


        }

    }

    public static void checkHistory() {

        List<SuhatChain> suhatChains = blockChain
                .stream()
                .filter(suhatChain -> suhatChain.data.getFrom().equals(pickedNasabah.getName()) || suhatChain.data.getTo().equals(pickedNasabah.getName()))
                .collect(Collectors.toList());

        System.out.println("=== Transaction History ===");

        if (suhatChains.size() <= 0) {
            System.out.println("There is no transaction record");
        } else {
            for (int i = 0; i < suhatChains.size(); i++) {
                System.out.print("\n");
                System.out.printf("Type : %s \n", suhatChains.get(i).data.getTransactionType());
                System.out.printf("From : %s \n", suhatChains.get(i).data.getFrom());
                System.out.printf("To : %s \n", suhatChains.get(i).data.getTo());
                System.out.printf("Amount : %d \n", suhatChains.get(i).data.getAmount());
                Date recordedAt = new Date(suhatChains.get(i).timestamp);
                System.out.printf("Recorded At : %s \n", dateFormat.format(recordedAt));
                System.out.print("\n");
            }
        }

        promptMenu();

    }


}
