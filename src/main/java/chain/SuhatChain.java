package chain;

import dao.TransactionDao;
import utils.StringUtil;

import java.util.Date;

public class SuhatChain {

    public String hash;
    public String prevHash;
    public TransactionDao data;
    public long timestamp;

    public SuhatChain(String prevHash, TransactionDao data) {
        this.prevHash = prevHash;
        this.data = data;
        this.timestamp = new Date().getTime();
        this.hash = calculateHash();
    }

    public String calculateHash() {
        return StringUtil.applySha256(prevHash +
                Long.toString(timestamp) +
                data);
    }

}
